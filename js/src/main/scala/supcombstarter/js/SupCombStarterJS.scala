package supcombstarter.js

import cats.effect.{ExitCode, IO, IOApp}
import outwatch.dom._
import outwatch.dom.dsl._
import monix.execution.Scheduler.Implicits.global

object SupCombStarterJS extends IOApp {
  def run(args: List[String]): IO[ExitCode] =
    for {
      topComponent <- TopComponent.init
      _ <- OutWatch.renderReplace(
        "#main",
        div(ModifierStreamReceiver(ValueObservable(topComponent.node))),
      )
    } yield ExitCode.Success
}
