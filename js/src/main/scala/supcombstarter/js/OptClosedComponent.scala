package supcombstarter.js

import cats.effect.IO
import cats.instances.either._
import cats.instances.option._
import cats.syntax.traverse._
import monix.reactive.Observable
import mouse.boolean._
import outwatch.Handler
import outwatch.dom.VDomModifier
import outwatch.dom.dsl._

final case class OptClosedComponent private (boolStream: Handler[Boolean]) {

  private def addButton =
    div(
      cls := "container",
      marginTop := "50px",
      div(
        cls := "form-row align-items-center",
        div(
          cls := "col-auto",
          button(
            "Add",
            `type` := "submit",
            cls := "btn btn-primary mb-2",
            onClick(true) --> boolStream,
          ),
        ),
      ),
    )

  private def removeButton =
    div(
      cls := "container",
      marginTop := "50px",
      div(
        cls := "form-row align-items-center",
        div(
          cls := "col-auto",
          button(
            "Remove",
            `type` := "submit",
            cls := "btn btn-primary mb-2",
            onClick(false) --> boolStream,
          ),
        ),
      ),
    )

  def nodeStream: Observable[VDomModifier] =
    for {
      bool <- boolStream
      maybeClosedComponentAttempt <- Observable.from(
        bool.option(ClosedComponent.init.attempt).sequence.map(_.sequence),
      )
      optString <- maybeClosedComponentAttempt.fold(
        throwable => Observable(s"Error: ${throwable.getMessage}"),
        _.traverse(_.valueStream).map(_.toString),
      )
    } yield
      VDomModifier(
        maybeClosedComponentAttempt.fold(
          { throwable =>
            org.scalajs.dom.console.error(throwable.getMessage)
            addButton
          },
          _.fold(
            addButton: VDomModifier,
          )(component =>
            VDomModifier(
              component.node,
              removeButton,
            ),
          ),
        ),
        div(
          cls := "container",
          marginTop := "50px",
          div(
            cls := "form-row align-items-center",
            optString,
          ),
        ),
      )

}

object OptClosedComponent {

  def init: IO[OptClosedComponent] =
    for {
      boolStream <- Handler.create[Boolean](false)
    } yield OptClosedComponent(boolStream)

}
