package supcombstarter.js

import cats.effect.IO
import monix.reactive.Observable
import outwatch.dom.{VDomModifier, VNode}

final case class TopComponent(
  helloNode: VNode,
  optClosedComponent: OptClosedComponent,
  listClosedComponent: ListClosedComponent,
) {

  def node: Observable[VDomModifier] =
    for {
      optClosedComponentNode  <- optClosedComponent.nodeStream
      listClosedComponentNode <- listClosedComponent.nodeStream
    } yield
      VDomModifier(
        helloNode,
        optClosedComponentNode,
        listClosedComponentNode,
      )

}

object TopComponent {

  def init: IO[TopComponent] =
    for {
      helloNode           <- new HelloComponent().node
      optClosedComponent  <- OptClosedComponent.init
      listClosedComponent <- ListClosedComponent.init
    } yield TopComponent(helloNode, optClosedComponent, listClosedComponent)

}
