package supcombstarter.jvm.config

import pureconfig.ConfigReader
import pureconfig.generic.semiauto._

final case class SlickConfig(
  db: SlickDbConfig,
)

object SlickConfig {
  implicit val slickConfigReader: ConfigReader[SlickConfig] =
    deriveReader[SlickConfig]
}

final case class SlickDbConfig(
  url: String,
  user: String,
  password: String,
)

object SlickDbConfig {
  implicit val slickDbConfigReader: ConfigReader[SlickDbConfig] =
    deriveReader[SlickDbConfig]
}
