package supcombstarter.jvm.data.db

import supcombstarter.jvm.data.db.SlickPgProfile.api._
import io.circe.{Decoder, Encoder, Json}
import slick.dbio.DBIO
import slick.lifted.{MappedProjection, ProvenShape}
import cats.syntax.either._
import cats.syntax.option._
import supcombstarter.jvm.models.lambda.{SCTerm, SCTermData, SCTermReference}

object SCTermDBIO {

  final class DataTable(tag: Tag) extends Table[SCTerm](tag, "supcomb") {

    def id = column[SCTerm.Id]("id", O.AutoInc, O.PrimaryKey)
    def text = column[String]("text")
    def exprType = column[String]("type")
    def referenceJson = column[Json]("reference")

    def reference: MappedProjection[Option[SCTermReference], Json] =
      referenceJson <> (
        Decoder[Option[SCTermReference]]
          .decodeJson(_).valueOr(throw _),
        Encoder[Option[SCTermReference]](implicitly)(_).some,
      )

    def data =
      (
        text,
        exprType,
        reference,
      ) <>
        ((SCTermData.apply _).tupled, SCTermData.unapply)

    def * : ProvenShape[SCTerm] =
      (
        id,
        data,
      ) <>
        ((SCTerm.apply _).tupled, SCTerm.unapply)
  }

  object Queries {
    val table = TableQuery(new DataTable(_))

    def schema = table.schema

    val data = Compiled(table.map(_.data))

    val byId = Compiled((id: Rep[SCTerm.Id]) => table.filter(_.id === id))

    val term = TableQuery[SCTermDBIO.DataTable]
  }

  def byId(id: SCTerm.Id): DBIO[Option[SCTerm]] =
    Queries.byId(id).result.headOption

  def insert(data: SCTermData): DBIO[SCTerm.Id] =
    Queries.data.returning(Queries.table.map(_.id)) += data

  def update(term: SCTerm): DBIO[Int] =
    Queries.table.update(term)

  def delete(id: SCTerm.Id): DBIO[Int] =
    Queries.byId(id).delete
}
