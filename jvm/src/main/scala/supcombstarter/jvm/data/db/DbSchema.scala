package supcombstarter.jvm.data.db

object DbSchema {
  lazy val createStatementsList =
    //NoteDBIO.Queries.schema.createStatements.toList
    SCTermDBIO.Queries.schema.createStatements.toList
}
