package supcombstarter.jvm

import java.nio.charset.StandardCharsets
import java.nio.file.{Files, Paths}
import java.time.{ZoneOffset, ZonedDateTime}

object MigrationInitializer {

  private def templateName(dt: ZonedDateTime) =
    s"V${dt.getYear}" +
      s"_${dt.getMonthValue.formatted("%02d")}" +
      s"_${dt.getDayOfMonth.formatted("%02d")}" +
      s"_${dt.getHour.formatted("%02d")}" +
      s"_${dt.getMinute.formatted("%02d")}" +
      s"_${dt.getSecond.formatted("%02d")}" +
      s"__description"

  def templateCode(dt: ZonedDateTime): String =
    s"""-- Please put some description in migration name instead of `description` part
       |-- Please put migration SQL code in this file
       |-- Current DB schema create SQL script can be generated from sbt:
       |--    project supcombstarterJVM
       |--    runMain supcombstarter.jvm.DbSchemaDumper
       |""".stripMargin

  def templateCode: String = templateCode(ZonedDateTime.now(ZoneOffset.UTC))

  def main(args: Array[String]): Unit = {
    val dt = ZonedDateTime.now(ZoneOffset.UTC)
    val path =
      Paths.get(s"jvm/src/main/resources/db/migration/${templateName(dt)}.sql")
    Files.write(
      path,
      templateCode(dt).getBytes(StandardCharsets.UTF_8),
    )
    println(s"created $path")
  }

}
