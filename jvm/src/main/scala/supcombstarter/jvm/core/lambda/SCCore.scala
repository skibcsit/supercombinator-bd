package supcombstarter.jvm.core.lambda

import supcombstarter.jvm.models.lambda._

object SCCore {
  def Apply(
    SCTermReference: SCTermReference,
    variables: List[SCVariable],
  ): SCTermReference =
    variables.headOption.fold(SCTermReference)(x =>
      Apply(SCApplication(SCTermReference, x), variables.tail),
    )

  def Lifting(
    term: Term,
    boundedVariables: Set[Variable],
    freeVariables: Set[Variable],
    isRec: Boolean = false,
    name: Option[Variable] = None,
  ): (SCTermReference, Set[Variable], Set[Variable], Option[List[SCVariable]]) =
    term match {
      case Application(function, argument) =>
        val (functionNew, functionBound, functionFree, _) =
          Lifting(function, boundedVariables, freeVariables)
        val (argumentNew, argumentBound, argumentFree, _) =
          Lifting(argument, boundedVariables, freeVariables)

        (
          SCApplication(functionNew, argumentNew),
          functionBound ++ argumentBound,
          functionFree ++ argumentFree,
          None,
        )

      case variable: Variable =>
        if (boundedVariables contains variable)
          (SCVariable(variable.name), boundedVariables, freeVariables, None)
        else
          (
            SCVariable(variable.name),
            boundedVariables,
            freeVariables + variable,
            None,
          )

      case Abstraction(variable, term) =>
        val abstractionBounded =
          name.map(Set(variable, _)).getOrElse(Set(variable))
        val (newSC, newBounded, newFree, _) =
          if (isRec)
            Lifting(
              term,
              boundedVariables ++ abstractionBounded,
              freeVariables,
              true,
            )
          else
            Lifting(term, abstractionBounded, freeVariables, true)
        val func: SCDefinition = newSC match {
          case SCDefinition(vars, term, _) =>
            SCDefinition(SCVariable(variable.name) +: vars, term)
          case default => SCDefinition(List(SCVariable(variable.name)), newSC)
        }
        val newNewBound = newBounded -- abstractionBounded
        if (newNewBound isEmpty) {
          val vars = newFree.toList.map(x => SCVariable(x.name))
          (
            Apply(
              name
                .map(x =>
                  SCDefinition(
                    vars ++ func.variables,
                    func.term,
                    x.name,
                  ),
                ).getOrElse(
                  SCDefinition(
                    vars ++ func.variables,
                    func.term,
                  ),
                ),
              vars,
            ),
            freeVariables,
            newNewBound,
            Some(vars),
          )
        } else
          (func, newNewBound, newFree, None)

    }

  def Lift(term: Term): SCTermReference = {
    val (newTerm, _, _, _) = Lifting(term, Set[Variable](), Set[Variable]())
    newTerm
  }
}
