package supcombstarter.jvm.core.inference

import supcombstarter.jvm.models.inference.{
  Arrow,
  Subst,
  Type,
  TypeScheme,
  Tyvar,
}
import supcombstarter.jvm.models.lambda.{
  SCApplication,
  SCDefinition,
  SCTermReference,
  SCVariable,
}

object TypeInferencer {

  type Env = Map[SCVariable, TypeScheme]

  private var n: Int = 0

  private var current: Option[SCTermReference] = None

  def newTyvar(): Type = {
    n += 1
    Tyvar("a" + n)
  }

  private def lookup(env: Env, x: SCVariable): Option[TypeScheme] = env.get(x)

  def gen(env: Env, t: Type): TypeScheme =
    TypeScheme(tyvars(t) diff tyvars(env), t)

  private def tyvars(t: Type): List[Tyvar] = t match {
    case tv @ Tyvar(a) => List(tv)
    case Arrow(t1, t2) => tyvars(t1) ++ tyvars(t2)
  }

  private def tyvars(ts: TypeScheme): List[Tyvar] =
    tyvars(ts.tpe) diff ts.tyvars

  private def tyvars(env: Env): List[Tyvar] =
    env.values.foldLeft(List[Tyvar]())((tvs, nt) => tvs ++ tyvars(nt))

  private def unify(t: Type, u: Type, s: Subst): Subst = (s(t), s(u)) match {
    case (Tyvar(a), Tyvar(b)) if a == b           => s
    case (Tyvar(a), _) if !(tyvars(u) contains a) => s.extend(Tyvar(a), u)
    case (_, Tyvar(a))                            => unify(u, t, s)
    case (Arrow(t1, t2), Arrow(u1, u2))           => unify(t1, u1, unify(t2, u2, s))
    case _                                        => throw new Exception(s"cannot unify ${s(t)} with ${s(u)}")
  }

  private def tp(env: Env, e: SCTermReference, t: Type, s: Subst): Subst = {
    current = Some(e)
    e match {
      case SCVariable(x) =>
        val u = lookup(env, SCVariable(x))
        if (u.isEmpty) throw new Exception("undefined: " + x)
        else unify(u.get.newInstance(), t, s)
      case SCDefinition(e1, e2, x) =>
        val a, b = newTyvar()
        val s1 = unify(t, Arrow(a, b), s)
        val env1 = env ++ Map((e1.head, TypeScheme(List.empty, a)))
        if (e1.tail.isEmpty) tp(env1, e2, b, s1)
        else tp(env1, SCDefinition(e1.tail, e2, x), b, s1)
      case SCApplication(e1, e2) =>
        val a = newTyvar()
        val s1 = tp(env, e1, Arrow(a, t), s)
        tp(env, e2, a, s1)
      /*case SCDefinition(e1, e2, x) =>
        val a = newTyvar()
        val s1 = tp(env, e1.head, a, s)
        if (e1.tail.isEmpty) tp(Map((SCVariable(x), gen(env, s1(a)))) ++ env, e2, t, s1)
        else tp(Map((SCVariable(x), gen(env, s1(a)))) ++ env, SCDefinition(e1.tail, e2, x), t, s1)*/
    }
  }

  def typeOf(env: Env, e: SCTermReference): Type = {
    val a = newTyvar()
    tp(env, e, a, Subst.empty())(a)
  }
}
