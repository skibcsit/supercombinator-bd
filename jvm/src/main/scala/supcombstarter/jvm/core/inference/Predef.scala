package supcombstarter.jvm.core.inference

import supcombstarter.jvm.models.inference.{Type, TypeScheme}
import supcombstarter.jvm.models.lambda.SCVariable

object Predef {

  private[this] def gen(t: Type): TypeScheme =
    TypeInferencer.gen(Map.empty, t)

  private val a = TypeInferencer.newTyvar()
  private val b = TypeInferencer.newTyvar()

  val env = Map(
    SCVariable("x") -> gen(a),
    SCVariable("y") -> gen(b),
  )
}
