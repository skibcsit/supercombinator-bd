## Запуск sbt
    sbt

## Сервер (JVM)

Запуск:

    reStart --- -Dconfig.resource=application.local.conf

Применение изменений:

    ~reStart --- -Dconfig.resource=application.local.conf

## БД

Создание SQL-схемы:

    project supcombstarterJVM
    runMain supcombstarter.jvm.DbSchemaDumper

Создание миграций:

    project supcombstarterJVM
    runMain supcombstarter.jvm.MigrationInitializer

## Обращение к приложению

По URL  http://127.0.0.1:8080